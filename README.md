# scotland-covid-case-distribution-random-forest-model

Underlying code for manuscript "Spatio-temporal characteristics of the SARS-CoV-2 Omicron variant spread at fine geographical scales, and comparison to earlier variants"