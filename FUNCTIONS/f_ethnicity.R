# Simple function to parse/output DZ/age ethnicity

f_ethnicity <- function(){
  
  # Census table LC2101SC - Ethnic group by age
  BAME_DZ_age <- read.csv("/Users/awood310/Desktop/R/Vaccinations/data/zones_and_population/census_data_dz/LC2101SC.csv")
  
  BAME_DZ_age <- setNames(BAME_DZ_age[,c(1,2,3,5,6)], c("DZ","age_range","Total","WhiteScottish","WhiteOtherBritish"))
  BAME_DZ_age <- BAME_DZ_age[!BAME_DZ_age$DZ == "S92000003",]
  BAME_DZ_age <- BAME_DZ_age[!BAME_DZ_age$age_range == "All people",]
  BAME_DZ_age$BAME <- 1 - (BAME_DZ_age$WhiteOtherBritish+BAME_DZ_age$WhiteScottish) / BAME_DZ_age$Total
  BAME_DZ_age$BAME[is.na(BAME_DZ_age$BAME)==TRUE] <- 0

  BAME_DZ_age <- BAME_DZ_age[,c("DZ","age_range","BAME")]
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="0 to 15"] <- "0-4"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "0-4",]
  BAME_DZ_age_temp$age_range <- "5-9"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  BAME_DZ_age_temp$age_range <- "10-14"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="16 to 24"] <- "15-19"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "15-19",]
  BAME_DZ_age_temp$age_range <- "20-24"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="25 to 34"] <- "25-29"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "25-29",]
  BAME_DZ_age_temp$age_range <- "30-34"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="35 to 49"] <- "35-39"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "35-39",]
  BAME_DZ_age_temp$age_range <- "40-44"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  BAME_DZ_age_temp$age_range <- "45-49"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="50 to 64"] <- "50-54"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "50-54",]
  BAME_DZ_age_temp$age_range <- "55-59"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  BAME_DZ_age_temp$age_range <- "60-64"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  
  BAME_DZ_age$age_range[BAME_DZ_age$age_range=="65 and over"] <- "65-69"
  BAME_DZ_age_temp <- BAME_DZ_age[BAME_DZ_age$age_range == "65-69",]
  BAME_DZ_age_temp$age_range <- "70-74"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)
  BAME_DZ_age_temp$age_range <- "75+"
  BAME_DZ_age <- rbind(BAME_DZ_age, BAME_DZ_age_temp)

return(BAME_DZ_age)

}

